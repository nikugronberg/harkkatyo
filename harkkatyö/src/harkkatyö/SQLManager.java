package harkkatyö;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.sqlite.SQLiteConfig;

public class SQLManager {
	static private SQLManager sqlManager = null;
	private Connection c = null;
	private LogWriter log = new LogWriter();
	
	private int success = 0;
	private int failure = 0;
	
	
	public static SQLManager getInstance() {
		//Singleton SQLManagerin käyttämiseen muissa luokissa
		if(sqlManager == null) {
			sqlManager = new SQLManager();
		}
		return sqlManager;
	}
	
	public Connection getConnection() {
		//Singleton connection jakamiseen, koska memorydatabaseen voi olla vain yksi connection, eli ei voida luoda uusia connectioneita joka kerta
		try {
			if(c == null || c.isClosed()) {
				SQLiteConfig conf = new SQLiteConfig();
				conf.enforceForeignKeys(true);
				c = DriverManager.getConnection("jdbc:sqlite:", conf.toProperties());
				
				
		
				System.out.println("Yhteys haettu");
//				System.out.println(c.getMetaData().supportsBatchUpdates());
			}
		} catch (Exception e) {
			System.out.println("Yhteyden haku epäonnistui");
			//ehkä kaada ohjelma
		}
		return c;
	}
	
	public void saveDatabase() {
		//tallennetaan memorydatabase tiedostoksi
		c = getConnection();
		String fileName = "db.db";
		try {
			Statement stat = c.createStatement();
	
			stat.executeUpdate("backup to '" + fileName + "'");
			log.writeToLog("Tietokanta tallennettu tiedostoon: " + fileName);
		} catch(SQLException sqle) {
			log.writeToLog("Tietokannan tallentaminen tiedostoon: " + fileName + " epäonnistui");
			System.out.println(sqle.getLocalizedMessage());
		}
	}
	
	public boolean loadDatabase() {
		//yrittää ladata databasen tiedostosta, onnistuessa palauttaa true, jos tiedostoa ei ole tai avaaminen epäonnistuu palauttaa false
		c = getConnection();
		String fileName = "db.db";
		try {
			Statement stat = c.createStatement();
	
			File dbFile = new File(fileName);
			if(dbFile.exists()) {
				stat.executeUpdate("restore from '" + fileName + "'");
				log.writeToLog("Tietokanta luettu tiedostosta: " + fileName);
				stat.close();
				return true;
			}
		} catch(SQLException sqle) {
			log.writeToLog("Tietokannan lukeminen tiedosta: " + fileName + " epäonnistui");
			System.out.println(sqle.getLocalizedMessage());
		}
		return false;
	}
	
	public boolean createDatabase() {
		//metodi databasen luomiseen, palauttaa true jos tietokanta luettu tiedostosta, muuten false
		boolean loadedFromFile = false;
		try {
			Class.forName("org.sqlite.JDBC");
			c = getConnection();
			
			if(loadedFromFile = loadDatabase()) {
				System.out.println("Tietokanta luettu tiedostosta");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return loadedFromFile;
	}
	
	public void fillDatabaseFromFile(String url) {
		//lukee sql tiedostoja fileManagerin kautta ja suorittaa tiedoston sisältämät käskyt
		try {
			c = getConnection();
			Statement statement = c.createStatement();
			FileManager fm = new FileManager();
			String content = fm.readFile(url);
			for(String line : content.split(";")) {
				statement.addBatch(line + ";");
			}
			statement.executeBatch();
			System.out.println("Asioita lisätty onnistuneesti");
			log.writeToLog("Tietokantaan lisätty asioita tiedostosta: " + url);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			log.writeToLog("Tietokantaan asioiden lisääminen epäonnistui tiedostosta: " + url);
		}
	}
	
	public void fillDatabaseWithBoxAutomaton() {
		//lisää pakettiautomaatit listasta tietokantaan ja laskee onnistumisprosentin (vielä ohjelman alkuvaiheissa se ei ollut 100)
		ListManager lm = ListManager.getInstance();
		int i = 0;
		for(Pakettimaatti p : lm.getAl()) {
			i++;
			System.out.println("Lisätään " + i);
			addBoxAutomatonToDatabase(p);
		}
		double ratio = success/(double)(success + failure);
		System.out.println("onnistuneita: " + success + "\nepäonnistuneita: " + failure + "\nonnistumisprosentti " + ratio);
		log.writeToLog("Tietokantaan lisätty automaatteja. Onnistuneita: " + success + " epäonnistuneita: " + failure);
	}
	
	public void addBoxAutomatonToDatabase(Pakettimaatti p) {
		//ottaa pakettimaatin ja jakaa se on osiin, jotka voidaan tallentaa tietokantaan
		//jokaista pakettiautomaattia kohden tehdään 3 INSERT:iä, joten tämä ratkaisu ei ole erityisen hyvä
		c = getConnection();
		String automaatti = "INSERT INTO automaatti(lat, lng, osoite, postinumero) VALUES (?,?,?,?)";
		String rakennus = "INSERT INTO rakennus(osoite, postinumero, kaupunki, rakennus, tyyppi) VALUES (?,?,?,?,?)";
		String aukioloaika = "INSERT INTO aukioloaika(lat, lng, päivä, kelloalku, kelloloppu) VALUES (?,?,?,?,?)";
		
		try {
			PreparedStatement buildingStatement = c.prepareStatement(rakennus);
			buildingStatement.setString(1, p.getOsoite());
			buildingStatement.setString(2, p.getPostinumero());
			buildingStatement.setString(3, p.getKaupunki());
			buildingStatement.setString(4, p.getRakennus());
			buildingStatement.setString(5, p.getTyyppi());
			buildingStatement.executeUpdate();
			
			PreparedStatement automatonStatement = c.prepareStatement(automaatti);
			automatonStatement.setDouble(1, p.getLat());
			automatonStatement.setDouble(2, p.getLng());
			automatonStatement.setString(3, p.getOsoite());
			automatonStatement.setString(4, p.getPostinumero());
			automatonStatement.executeUpdate();
			
			String d = "";
			String ts = "";
			String te = "";
			for(Entry<String, String> e : p.getAika().entrySet()) {
				if(e.getValue().contains("-")) {
					d = e.getKey();
					ts = e.getValue().split("-")[0];
					te = e.getValue().split("-")[1];
				} else {
					d = e.getKey();
					ts = e.getValue();
					te = e.getValue();
				}
				
				PreparedStatement openBeingTimeStatement = c.prepareStatement(aukioloaika);
				openBeingTimeStatement.setDouble(1, p.getLat());
				openBeingTimeStatement.setDouble(2, p.getLng());
				openBeingTimeStatement.setString(3, d);
				openBeingTimeStatement.setString(4, ts);
				openBeingTimeStatement.setString(5, te);
				openBeingTimeStatement.executeUpdate();
			}
			success++;
		} catch(SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
		} catch(Exception e) {
			System.out.println(p.getKaupunki() + " " + p.getAika().toString());
			failure++;
		}
	}
	
	public void printStats() {
		//jäänne
		System.out.println("Onnistuneita: " + success + " Epäonnistuneita: " + failure + "\nOnnistumisprosentti: " + success/(double) (success + failure));
	}
	
	public LinkedHashMap<String, String> getTime(double lat, double lng, Connection c) {
		//ajan hakeminen tietokannasta ja sen parsiminen oikeaan muotoon
		LinkedHashMap<String, String> h = new LinkedHashMap<String, String>();
		try {
			ResultSet rs = null;
			String sql = "SELECT * FROM aukioloaika WHERE lat = ? AND lng = ?";
			PreparedStatement s = c.prepareStatement(sql);
			s.setDouble(1, lat);
			s.setDouble(2, lng);
			long st = System.currentTimeMillis();
			rs = s.executeQuery();
			long end = System.currentTimeMillis();
			System.out.println("Query: " + (end - st) + "ms");
			while(rs.next()) {
				if(rs.getString("kelloalku").equals("kiinni") || rs.getString("kelloalku").equals("24h")) {
					h.put(rs.getString("päivä"), rs.getString("kelloalku"));
				} else {
					h.put(rs.getString("päivä"), rs.getString("kelloalku") + "-" + rs.getString("kelloloppu"));
				}
			}
			
			
		} catch (SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
		}
	return h;
	}
	
	public void getItems() {
		//hakee kaikki esineen tietokannasta ja lisää ne arrayListaan
		c = getConnection();
		ListManager listMan = ListManager.getInstance();
		String sql = "SELECT * FROM esine";	
			try {
				Statement s = c.createStatement();
				ResultSet rs = s.executeQuery(sql);

				while(rs.next()) {
					int id = rs.getInt("id");
					String nimi = rs.getString("nimi");
					double pituus = rs.getDouble("pituus");
					double leveys = rs.getDouble("leveys");
					double korkeus = rs.getDouble("korkeus");
					double paino = rs.getDouble("paino");
					int hajoava = rs.getInt("hajoava");
					String muuta = rs.getString("muuta");
					
					Items item = new Items(id, nimi, pituus, leveys, korkeus, paino, hajoava, muuta);
					listMan.getIl().add(item);
					System.out.println("Lisätty listaan: " + item.getNimi());
					
				}
				System.out.println(listMan.getIl().size());
				log.writeToLog("Tietokannasta haettu esineitä " + listMan.getIl().size() + "kpl");
				
			} catch (SQLException sqle) {
				System.out.println(sqle.getLocalizedMessage());
				log.writeToLog("Esineiden hakeminen tietokannasta epäonnistui");
			}
	}
	
	public void getSendClass() {
		//hakee kaikki lähetysluokat tietokannasta ja lisää ne ArrayListaan
		c = getConnection();
		ListManager listMan = ListManager.getInstance();
		String sql = "SELECT * FROM luokka";	
			try {
				Statement s = c.createStatement();
				ResultSet rs = s.executeQuery(sql);

				while(rs.next()) {
					int luokka = rs.getInt("luokka");
					double kokoraja = rs.getDouble("kokoraja");
					double painoraja = rs.getDouble("painoraja");
					double etäisyysraja = rs.getDouble("etäisyysraja");
					int nopeus = rs.getInt("nopeus");
					int hajottava = rs.getInt("hajottava");
					
					SendClass sc = new SendClass(luokka, kokoraja, painoraja, etäisyysraja, nopeus, hajottava);
					listMan.getScl().add(sc);
					System.out.println("Lisätty listaan: " + sc.getLuokka());
					
				}
				System.out.println(listMan.getScl().size());
				log.writeToLog("Tietokannasta haettu luokkia " + listMan.getScl().size() + "kpl");
				
			} catch (SQLException sqle) {
				System.out.println(sqle.getLocalizedMessage());
				log.writeToLog("Luokkien hakeminen tietokannasta epäonnistui");
			}
	}
	
	public void getAllAutomaton() {
		//hakee kaikki pakettiautomaatit tietokannasta ja lisää ne arrayListaan
		c = getConnection();
		ListManager listMan = ListManager.getInstance();
		listMan.getAl().clear();
		String sql = "SELECT * FROM haePakettimaatti";
		try {
			Statement s = c.createStatement();
			ResultSet rs = s.executeQuery(sql);

			while(rs.next()) {
				double lat = rs.getDouble("lat");
				double lng = rs.getDouble("lng");
				String osoite = rs.getString("osoite");
				String rakennus = rs.getString("rakennus");
				String tyyppi = rs.getString("tyyppi");
				String kaupunki = rs.getString("kaupunki");
				String postinumero = rs.getString("postinumero");
				
				LinkedHashMap<String, String> aika = getTime(lat, lng, c);
				
				Pakettimaatti p = new Pakettimaatti(lat, lng, osoite, rakennus, tyyppi, kaupunki, postinumero, aika);
				listMan.getAl().add(p);
				System.out.println("Lisätty listaan: " + p.getKaupunki());
				
			}
			System.out.println(listMan.getAl().size());
			log.writeToLog("Tietokannasta haettu pakettiautomaatteja " + listMan.getAl().size() + "kpl");
			
		} catch (SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Automaattien hakeminen tietokannasta epäonnistui");
		}
	}
	
	public void addItemToDatabase(Items item) {
		//lisää yksittäisen esineen tietokantaan
		String sql = "INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta) VALUES (?,?,?,?,?,?,?)";
		c = getConnection();
		try {
			PreparedStatement pstat = c.prepareStatement(sql);
			pstat.setString(1, item.getNimi());
			pstat.setDouble(2, item.getPituus());
			pstat.setDouble(3, item.getLeveys());
			pstat.setDouble(4, item.getKorkeus());
			pstat.setDouble(5, item.getPaino());
			pstat.setInt(6, item.getHajoava());
			pstat.setString(7, item.getMuuta());
			pstat.executeUpdate();
			
			log.writeToLog("Tietokantaan lisätty: " + item.getClass() + " " + item.getNimi());
		} catch(SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Tietokantaan lisääminen epäonnistui: " + item.getClass() + " " + item.getNimi());
		}
	}
	
	public void updateItemInDatabase(Items item) {
		//päivittää esineen tiedot tietokannassa
		String sql = "UPDATE esine SET nimi = ?, pituus = ?, leveys = ?, korkeus = ?, paino = ?, hajoava = ?, muuta = ? WHERE id = ?";
		c = getConnection();
		try {
			PreparedStatement pstat = c.prepareStatement(sql);
			pstat.setString(1, item.getNimi());
			pstat.setDouble(2, item.getPituus());
			pstat.setDouble(3, item.getLeveys());
			pstat.setDouble(4, item.getKorkeus());
			pstat.setDouble(5, item.getPaino());
			pstat.setInt(6, item.getHajoava());
			pstat.setString(7, item.getMuuta());
			pstat.setInt(8, item.getId());
			pstat.executeUpdate();
			
			log.writeToLog("Tietokantaan päivitetty: " + item.getClass() + " " + item.getNimi());
		} catch(SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Tietokantaan päivittäminen epäonnistui: " + item.getClass() + " " + item.getNimi());
		}
	}
	
	public void deleteItemFromDatabase(Items item) {
		//poistaa esineen tietokannasta
		String sql = "DELETE FROM esine WHERE id = ?";
		c = getConnection();
		try {
			PreparedStatement pstat = c.prepareStatement(sql);
			pstat.setInt(1, item.getId());
			pstat.executeUpdate();
			
			log.writeToLog("Tietokannasta poistettu: " + item.getClass() + " " + item.getNimi());
		} catch (SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Tietokannasta poistaminen epäonnistui: " + item.getClass() + " " + item.getNimi());
		}
	}
	
	public void addPileToDatabase(Package p, int i) {
		//lisää kasan eli kokoelman esineitä tietokantaan
		String sql = "INSERT INTO kasa(esineid, pakettiid) VALUES (?,?)";
		c = getConnection();
		//lisättyjen esineiden määrä
		int amount = 0;
		try {
			PreparedStatement pstat = null;
			for(Items item : p.getItems()) {
				pstat = c.prepareStatement(sql);
				pstat.setInt(1, item.getId());
				pstat.setInt(2, i);
				pstat.executeUpdate();
				System.out.println("Lisätään: " + item.getNimi() + " id: " + item.getId() + " paketti id: " + i);	
				amount++;
			}
			log.writeToLog("Tietokantaan lisätty: " + "kasa" + ", esineitä: " + amount + "kpl");
		} catch(SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Tietokantaan lisääminen epäonnistui: " + "kasa" + ", esineitä: " + amount + "kpl");
		}
		
	}
	
	public int addPackageToDatabase(SendClass sc) {
		//lisää paketin tietokantaan
		String sql = "INSERT INTO paketti(luokka) VALUES (?)";
		c = getConnection();
		try {
			PreparedStatement pstat = c.prepareStatement(sql);
			pstat.setInt(1, sc.getLuokka());
			pstat.executeUpdate();
			int i = pstat.getGeneratedKeys().getInt(1);
			
			System.out.println("Lisätään paketti tietokantaan id=" + i);
			log.writeToLog("Tietokantaan lisätty: " + "Package" + " " + i);
			return i;
		} catch(SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Tietokantaan lisääminen epäonnistui: " + "Package");
		}
		return 0;
		
	}
	
	public void getAllPackageFromDatabase() {
		//hakee kaikki kasat tietokannast ja laittaa ne listaan. Kasa etsii esineensä esinelistasta ja yhdistää ne esine id:llä
		c = getConnection();
		ListManager listMan = ListManager.getInstance();
		String sql = "SELECT * FROM kasa JOIN esine ON esineid = esine.id";	
			try {
				Statement s = c.createStatement();
				ResultSet rs = s.executeQuery(sql);
				
				int packId = 0;
				int itemId = 0;
				Package pack = null;
				
				while(rs.next()) {
					//jos paketti id on sama kuin viime kierroksella, niin samaan pakettiin lisätään esine
					if(packId == (packId = rs.getInt("pakettiid"))) {
						itemId = rs.getInt("esineid");
						for(Items item : listMan.getIl()) {
							if(item.getId() == itemId) {
								pack.addItem(item);
							}
						}
					} else {
						//jos ei ole ensimmäinen kierros, niin valmis paketti lisätään listaan
						if(pack != null) {
							listMan.getPl().add(pack);
						}
						//paketti id on muuttunut, joten luodaan uusi paketti
						pack = new Package();
						pack.setId(packId);
						itemId = rs.getInt("esineid");
						for(Items item : listMan.getIl()) {
							if(item.getId() == itemId) {
								pack.addItem(item);
							}
						}
					}	
				}
				//viimeinen paketti lisätään listaan
				if(pack != null) {
					listMan.getPl().add(pack);
				}
				System.out.println(listMan.getPl().size());
				log.writeToLog("Pakettien hakeminen tietokannasta onnistui");
				
			} catch (SQLException sqle) {
				System.out.println(sqle.getLocalizedMessage());
				log.writeToLog("Pakettien hakeminen tietokannasta epäonnistui");
			}
	}
	
	public int addRouteToDatabase(Pakettimaatti ps, Pakettimaatti pe) {
		//lisätään reitti tietokantaan. Reitti koostuu kahdesta pakettiautomaatista. Palauttaa tietokannan reitille määrittelemän id:n
		String sql = "INSERT INTO reitti(alkulat, alkulng, loppulat, loppulng) VALUES (?,?,?,?)";
		c = getConnection();
		try {
			PreparedStatement pstat = c.prepareStatement(sql);
			pstat.setDouble(1, ps.getLat());
			pstat.setDouble(2, ps.getLng());
			pstat.setDouble(3, pe.getLat());
			pstat.setDouble(4, pe.getLng());
			pstat.executeUpdate();
			
			log.writeToLog("Tietokantaan lisätty: " + "reitti" + " " + ps.getKaupunki() + "->" + pe.getKaupunki());
			return pstat.getGeneratedKeys().getInt(1);
		} catch(SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Tietokantaan lisääminen epäonnistui: " + "reitti" + " " + ps.getKaupunki() + "->" + pe.getKaupunki());
		}
		return 0;
	}
	
	public void deleteDeliveryFromDatabase(Delivery d) {
		//poistaa toimituksen tietokannasta. Tämä taphtuu poistamalla reitti ja paketti, joiden poistaminen CASCADEE ja poistaa toimituksen
		String sql = "DELETE FROM reitti WHERE id = ?";
		String sql2 = "DELETE FROM paketti WHERE id = ?";
		c = getConnection();

		try {
			PreparedStatement pstat = c.prepareStatement(sql);
			pstat.setInt(1, d.getReittiid());
			pstat.executeUpdate();
			pstat = c.prepareStatement(sql2);
			pstat.setInt(1, d.getPakettiid());
			pstat.executeUpdate();
			
			log.writeToLog("Tietokannasta poistettu: " + d.getClass() + " " + d.getPs().getKaupunki() + "->" + d.getPe().getKaupunki() + " paketti id: " + d.getPakettiid() + " reitti id: " + d.getReittiid());
		} catch(SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Tietokannasta poistaminen epäonnistui: " + d.getClass() + " " + d.getPs().getKaupunki() + "->" + d.getPe().getKaupunki());
		}
	}
	
	public void addDeliveryToDatabase(int route, int pack, String time) {
		//Lisää toimituksen tietokantaan
		String sql = "INSERT INTO toimitus(reittiid, pakettiid, aika) VALUES (?,?,?)";
		c = getConnection();
		try {
			PreparedStatement pstat = c.prepareStatement(sql);
			pstat.setInt(1, route);
			pstat.setInt(2, pack);
			pstat.setString(3, time);
			pstat.executeUpdate();
			
			log.writeToLog("Tietokantaan lisätty: " + "Delivery" + route + " " + pack);
		} catch(SQLException sqle) {
			System.out.println(sqle.getLocalizedMessage());
			log.writeToLog("Tietokantaan lisääminen epäonnistui: " + "Delivery" + route + " " + pack);
		}
	}
	
	public void getAllDeliveryFromDatabase() {
		//hakee kaikki toimitukset tietokannasta ja parsii ne muotoon, jossa ne voidaan lisätä listaan. Sitten lisää ne listaan
		//toimituksen paketin lisäämisen metodi on samanlainen kuin pakettien hakemisessa käytetty metodi
		c = getConnection();
		ListManager listMan = ListManager.getInstance();
		String sql = "SELECT * FROM haeToimitus";	
			try {
				Statement s = c.createStatement();
				ResultSet rs = s.executeQuery(sql);
				int id = 0;
				int reittiid = 0;
				ArrayList<Items> itemList = new ArrayList<Items>();
				Pakettimaatti ps = null;
				Pakettimaatti pe = null;
				SendClass sc = null;
				String time = "";
				if(!rs.isBeforeFirst()) {
					System.out.println("se on null");
					return;
				}
				while(rs.next()) {
					if(id != rs.getInt("pakettiid")) {
						if(id != 0) {
							Delivery d = new Delivery(ps, pe, sc, itemList, time, id, reittiid);
							listMan.getDl().add(d);
							System.out.println("Uusi toimitus lisätty listaan");
						}
						id = rs.getInt("pakettiid");
						reittiid = rs.getInt("reittiid");
						System.out.println("Uusi kokonaisuus");
						ps = listMan.getAutomaton(rs.getDouble("alkulat"), rs.getDouble("alkulng"));
						pe = listMan.getAutomaton(rs.getDouble("loppulat"), rs.getDouble("loppulng"));
						sc = listMan.getSendClass(rs.getInt("luokka"));
						time = rs.getString("aika");
						Items item = listMan.getItem(rs.getInt("esineid"));
						itemList = new ArrayList<Items>();
						if(item == null) {
							//koska tietokannasta voidaan poistaa esineitä, niin poistetun esineen kohdalla käyttäjälle kuitenkin näytetään että
							//esine on ollut olemassa, mutta se on poistettu, joten luodaan uusi poistettu esine
							item = new Items("esine poistettu", 0, 0, 0, 0, 0, "");
						}
						itemList.add(item);
					} else {
						Items item = listMan.getItem(rs.getInt("esineid"));
						if(item == null) {
							//poistetun esineen lisääminen, katso ylempi kommentti
							item = new Items("esine poistettu", 0, 0, 0, 0, 0, "");
						}
						itemList.add(item);
						System.out.println("Uusi esine");
					}
					

					
				}
				Delivery d = new Delivery(ps, pe, sc, itemList, time, id, reittiid);
				listMan.getDl().add(d);
				System.out.println("Uusi toimitus lisätty listaan");
				log.writeToLog("Tietokannasta haettu toimitukset");
				
				
			} catch (SQLException sqle) {
				System.out.println(sqle.getLocalizedMessage());
				log.writeToLog("Tietokannasta toimituksien hakeminen epäonnistui");
			}
	}
}
