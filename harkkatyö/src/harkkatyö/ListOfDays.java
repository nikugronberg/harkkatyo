package harkkatyö;

import java.util.ArrayList;

public final class ListOfDays {
	//lista päivistä. Luulin että tätä tarvittaisiin useammin, mutta tätä tarvitaankin vain kerran
	private static ArrayList<String> day;
	
	public static ArrayList<String> getListOfDays() {
		day = new ArrayList<String>();
		day.add("ma");
		day.add("ti");
		day.add("ke");
		day.add("to");
		day.add("pe");
		day.add("la");
		day.add("su");
		return day;
	}
}
