package harkkatyö;


import java.util.LinkedHashMap;

public class Pakettimaatti {
	//luokka pakettiautomaateille
	private double lat;
	private double lng;
	
	private String osoite;
	private String rakennus;
	private String tyyppi;
	
	private String kaupunki;
	private String postinumero;
	
	private LinkedHashMap<String, String> aika;
	
	public Pakettimaatti(double la, double ln, String o, String r, String t, String k, String p, LinkedHashMap<String, String> a) {
		lat = la;
		lng = ln;
		osoite = o;
		rakennus = r;
		tyyppi = t;
		kaupunki = k;
		postinumero = p;
		aika = a;
	}
	
	@Override public String toString() {
		//override metodille toString, jotta objecteja voisi lisätä helpommin comboBoxeihin ja listViewihin
		return kaupunki + " " + osoite;
	}

	public double getLat() {
		return lat;
	}

	public double getLng() {
		return lng;
	}
	
	public String getOsoite() {
		return osoite;
	}

	public String getRakennus() {
		return rakennus;
	}

	public String getTyyppi() {
		return tyyppi;
	}

	public String getKaupunki() {
		return kaupunki;
	}

	public String getPostinumero() {
		return postinumero;
	}

	public LinkedHashMap<String, String> getAika() {
		return aika;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public void setOsoite(String osoite) {
		this.osoite = osoite;
	}

	public void setRakennus(String rakennus) {
		this.rakennus = rakennus;
	}

	public void setTyyppi(String tyyppi) {
		this.tyyppi = tyyppi;
	}

	public void setKaupunki(String kaupunki) {
		this.kaupunki = kaupunki;
	}

	public void setPostinumero(String postinumero) {
		this.postinumero = postinumero;
	}


/*
	public void setPaiva(String paiva) {
		this.paiva = paiva;
	}

	public void setKelloAlku(String kelloAlku) {
		this.kelloAlku = kelloAlku;
	}

	public void setKelloLoppu(String kelloLoppu) {
		this.kelloLoppu = kelloLoppu;
	}
*/

}
