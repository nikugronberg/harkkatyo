
package harkkatyö;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;

/* Niku Grönberg kesä 2017 SPC
 * Harkkatyö
 */


public class Main extends Application {
	
	@Override
	public void start(Stage stage) throws Exception {
		Parent root = 
	FXMLLoader.load(getClass().getResource("UI.fxml"));
		
		LogWriter log = new LogWriter();
		
		//määritellään ikkunan minimikoot
		stage.setMinHeight(800);
		stage.setMinWidth(1200);
		
		Scene scene = new Scene(root);
		scene.getStylesheets().addAll(this.getClass().getResource("UI.css").toExternalForm());
		stage.setTitle("Timotein Seikkalut -Ogre Green Edition-");
		stage.setScene(scene);
		stage.show();
		//tietokanta tallennetaan tiedostoon kun ohjelma suljetaan
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				System.out.println("Tallennetaan tietokanta");
				SQLManager sqlMan = SQLManager.getInstance();
				sqlMan.saveDatabase();
				System.out.println("Suljetaan ohjelma");
				
				log.writeToLog("Ohjelma suljettu\n***\n");
			}
		});
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
