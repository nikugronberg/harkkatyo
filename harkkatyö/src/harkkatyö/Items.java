package harkkatyö;

public class Items {
	//luokka esineille
	private int id;
	private String nimi;
	private double pituus;
	private double leveys;
	private double korkeus;
	private double paino;
	private int hajoava;
	private String muuta;
	
	Items(int i, String n, double p, double l, double k, double pa, int h, String m) {
		id = i;
		nimi = n;
		pituus = p;
		leveys = l;
		korkeus = k;
		paino = pa;
		hajoava = h;
		muuta = m;
	}
	
	Items(String n, double p, double l, double k, double pa, int h, String m) {
		nimi = n;
		pituus = p;
		leveys = l;
		korkeus = k;
		paino = pa;
		hajoava = h;
		muuta = m;
	}
	
	Items(String n, String m) {
		nimi = n;
		muuta = m;
	}
	
	@Override public String toString() {
		//override toString metodi, jotta objecteja voisi lisätä comboBoxeihin ja listVieveihin helposti
		return nimi;
	}
	
	public int getId() {
		return id;
	}

	public String getNimi() {
		return nimi;
	}

	public double getPituus() {
		return pituus;
	}

	public double getLeveys() {
		return leveys;
	}

	public double getKorkeus() {
		return korkeus;
	}

	public double getPaino() {
		return paino;
	}

	public int getHajoava() {
		return hajoava;
	}

	public String getMuuta() {
		return muuta;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public void setPituus(double pituus) {
		this.pituus = pituus;
	}

	public void setLeveys(double leveys) {
		this.leveys = leveys;
	}

	public void setKorkeus(double korkeus) {
		this.korkeus = korkeus;
	}

	public void setPaino(double paino) {
		this.paino = paino;
	}

	public void setHajoava(int hajoava) {
		this.hajoava = hajoava;
	}

	public void setMuuta(String muuta) {
		this.muuta = muuta;
	}

}
