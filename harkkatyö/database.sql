
PRAGMA foreign_keys = on;

/*TABLES*/
CREATE TABLE esine(
	id INTEGER PRIMARY KEY,
	nimi VARCHAR(40) NOT NULL,
	pituus REAL NOT NULL,
	leveys REAL NOT NULL,
	korkeus REAL NOT NULL,
	paino REAL NOT NULL,
	hajoava BOOLEAN DEFAULT 0,
	muuta VARCHAR(40)
);

CREATE TABLE kasa(
	id INTEGER PRIMARY KEY,
	esineid INTEGER,
	pakettiid INTEGER,
		
	FOREIGN KEY(esineid) REFERENCES esine(id) ON DELETE SET NULL,
	FOREIGN KEY(pakettiid) REFERENCES paketti(id) ON DELETE CASCADE
);

CREATE TABLE luokka(
	luokka INTEGER PRIMARY KEY NOT NULL,
	kokoraja REAL,
	painoraja REAL,
	etäisyysraja REAL,
	nopeus REAL,
	hajottava BOOLEAN
);

CREATE TABLE paketti(
	id INTEGER PRIMARY KEY,
	luokka INTEGER NOT NULL,
	
	FOREIGN KEY(luokka) REFERENCES luokka(luokka)
);

CREATE TABLE reitti(
	id INTEGER PRIMARY KEY,
	alkulat REAL NOT NULL,
	alkulng REAL NOT NULL,
	loppulat REAL NOT NULL,
	loppulng REAL NOT NULL,
	
	FOREIGN KEY(alkulat, alkulng) REFERENCES automaatti(lat, lng),
	FOREIGN KEY(loppulat, loppulng) REFERENCES automaatti(lat,lng)
	CHECK(alkulat != loppulat OR alkulng != loppulng)
);

CREATE TABLE automaatti(
	lat REAL NOT NULL,
	lng REAL NOT NULL,
	
	osoite VARCHAR(40),
	postinumero VARCHAR(6),
	
	PRIMARY KEY(lat, lng),
	FOREIGN KEY(osoite, postinumero) REFERENCES rakennus(osoite, postinumero)
);

CREATE INDEX PakettimaattiIdx ON automaatti(lat, lng);

CREATE TABLE aukioloaika(
	päivä CHAR(2) NOT NULL,
	kelloalku VARCHAR(5),
	kelloloppu VARCHAR(5),
	
	lat REAL NOT NULL,
	lng REAL NOT NULL,
	
	PRIMARY KEY(päivä, lat, lng),
	
	FOREIGN KEY(lat, lng) REFERENCES automaatti(lat, lng)
);

CREATE TABLE rakennus(
	kaupunki VARCHAR(20) NOT NULL,
	postinumero VARCHAR(6) NOT NULL,
	rakennus VARCHAR(40),
	tyyppi VARCHAR(40),
	osoite VARCHAR(40),
	
	PRIMARY KEY(osoite, postinumero)
);

CREATE TABLE toimitus(
	id INTEGER PRIMARY KEY NOT NULL,
	aika DATE,
	pakettiid INTEGER,
	reittiid INTEGER,
	
	FOREIGN KEY(pakettiid) REFERENCES paketti(id) ON DELETE CASCADE,
	FOREIGN KEY(reittiid) REFERENCES reitti(id) ON DELETE CASCADE
);

/*VIEWS*/
CREATE VIEW haeReitti AS
	SELECT toimitus.id AS 'toimitus id', 
	reitti.alkulat AS 'alkulat', 
	reitti.alkulng AS 'alkulng',
	alkurakennus.kaupunki AS 'alku', 
	reitti.loppulat AS 'loppulat', 
	reitti.loppulng AS 'loppulng', 
	loppurakennus.kaupunki AS 'loppu'
	
	FROM toimitus
	JOIN reitti ON reitti.id = reittiid
	
	JOIN automaatti AS alku 
	ON reitti.alkulat = alku.lat 
	AND reitti.alkulng = alku.lng
	
	JOIN automaatti AS loppu 
	ON reitti.loppulat = loppu.lat
	AND reitti.loppulng = loppu.lng
	
	JOIN rakennus AS alkurakennus 
	ON alkurakennus.osoite = alku.osoite 
	AND alkurakennus.postinumero = alku.postinumero
	
	JOIN rakennus AS loppurakennus 
	ON loppurakennus.osoite = loppu.osoite 
	AND loppurakennus.postinumero = loppu.postinumero
;

CREATE VIEW haePakettimaatti AS
	SELECT automaatti.lat AS 'lat',
	automaatti.lng AS 'lng',
	automaatti.osoite AS 'osoite',
	automaatti.postinumero AS 'postinumero',
	rakennus.rakennus AS 'rakennus',
	rakennus.tyyppi AS 'tyyppi',
	rakennus.kaupunki AS 'kaupunki'

	
	FROM automaatti
	JOIN rakennus ON automaatti.osoite = rakennus.osoite
	AND automaatti.postinumero = rakennus.postinumero
;


CREATE VIEW haePaketti AS
	SELECT toimitus.id AS 'toimitus id',
	luokka.luokka AS 'luokka',
	paketti.id AS 'paketti',
	esine.nimi AS 'nimi',
	esine.id AS 'esine'
	
	FROM toimitus
	JOIN paketti ON toimitus.pakettiid = paketti.id
	JOIN luokka ON paketti.luokka = luokka.luokka
	JOIN kasa ON paketti.id = kasa.pakettiid
	JOIN esine on kasa.esineid = esine.id 
;

CREATE VIEW haeAukiOloAika AS
	SELECT automaatti.lat AS 'lat',
	automaatti.lng AS 'lng',
	päivä AS 'päivä',
	kelloalku AS 'alku',
	kelloloppu AS 'loppu'
	FROM automaatti INNER JOIN aukioloaika ON
	automaatti.lat = aukioloaika.lat AND automaatti.lng = aukioloaika.lng
;

CREATE VIEW haeRakennus AS
	SELECT automaatti.lat AS 'lat',
	automaatti.lng AS 'lng',
	rakennus.kaupunki AS 'rakennus',
	rakennus.postinumero AS 'postinumero'
	FROM automaatti JOIN rakennus ON
	automaatti.osoite = rakennus.osoite AND automaatti.postinumero =  rakennus.postinumero
;

CREATE VIEW haeToimitus AS
	SELECT toimitus.id AS 'toimitusid',
	toimitus.aika AS 'aika',
	toimitus.pakettiid AS 'pakettiid',
	toimitus.reittiid AS 'reittiid',
	paketti.luokka AS 'luokka',
	kasa.esineid AS 'esineid',
	reitti.alkulat AS 'alkulat',
	reitti.alkulng AS 'alkulng',
	reitti.loppulat AS 'loppulat',
	reitti.loppulng AS 'loppulng'

	FROM toimitus 
	JOIN paketti ON toimitus.pakettiid = paketti.id
	JOIN luokka ON paketti.luokka = luokka.luokka
	JOIN kasa ON kasa.pakettiid = paketti.id
	LEFT OUTER JOIN esine ON kasa.esineid = esine.id
	JOIN reitti ON toimitus.reittiid = reitti.id
;

