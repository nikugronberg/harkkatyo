/*
mittayksikkö: metri/kuutiometri
painoyksikkö: kilogramma
nopeus: suhteellinen
-1 = ääretön
*/

/*LUOKAT*/
INSERT INTO luokka(luokka, kokoraja, painoraja, etäisyysraja, nopeus, hajottava)
	VALUES(1, 2, 20, 150000, 1, 1)
;

INSERT INTO luokka(luokka, kokoraja, painoraja, etäisyysraja, nopeus, hajottava)
	VALUES(2, 0.1, 50, -1, 2, 0)
;

INSERT INTO luokka(luokka, kokoraja, painoraja, etäisyysraja, nopeus, hajottava)
	VALUES(3, -1, -1, -1, 3, 1)
;


/*VALMIIT ESINEET*/
INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Postal gun', 0.5, 0.3, 0.4, 5, 0, 'Now you are thinking in postals')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Laatikollinen kiviä', 0.60, 0.30, 0.30, 140, 0, 'Joskus on pakko lähettää kiviä')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Kivellinen laatikoita', 0.30, 0.20, 0.20, 60, 0, 'äivik äättehäl okkap no suksoJ')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Kirje', 0.21, 0.15, 0.0003, 0.05, 0, 'Muista sukulaisia')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('???', 0.50, 0.20, 0.20, 2.0, 1, '???')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Kokonainen ikkunalasi', 2, 1.5, 0.004, 5, 1, 'Turvallisuusriski kaikille')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Puolikas ikkunalasi', 2, 0.75, 0.004, 2.5, 1, 'Pienempi turvallisuusriski kaikille')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Ihminen', 1.75, 0.60, 0.30, 90, 0, 'Postin harmaa-alue')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Companion Cube', 1, 1, 1, 10, 0, 'Enemmän kuin kuutio')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Iso nuudeli', 10, 0.01, 0.001, 10, 0, 'Suosittu ruoka nyt isompana')
;

INSERT INTO esine(nimi, pituus, leveys, korkeus, paino, hajoava, muuta)
	VALUES ('Kakku', 0.5, 0.5, 0.3, 3, 1, 'Kakku')
;













